Lowest Common Ancestor - Part 1
- Final Commit for LCA in Java implementation on the 06/10/2020
- Final Commit for LCA in Python implementation on the 12/10/2020

Lowest Common Ancestor - Part 2
- Final Commit for LCA and unit testing in Java implementation on the 18/10/2020
- Final Commit for LCA and unit testing Python implementation on the 18/10/2020

Lowest Common Ancestor - DAG
- Branched from master on 10/11/2020
- Merged to master with no conflicts on 11/11/2020