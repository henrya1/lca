# A binary tree node
class Node:

    # Constructor to create a new tree node
    def __init__(self, key):
        self.key = key
        self.predecessors = []
        self.successors = []


# This function returns pointer to LCA of two given
# values n1 and n2
# This function assumes that n1 and n2 are present in
# Binary Tree
def dag_find_LCA(root, n1, n2):
    if root is None:
        return None
    
    if n1 is None and n2 is None:
        return root.key
    
    if n1 is None:
        return n2.key

    if n2 is None:
        return n1.key

    if root.key == n1.key or root.key == n2.key:
        return root.key

    if n1.key == n2.key:
        return n1.key
    lca = []

    for i in range(len(n1.predecessors)):
        for j in range(len(n2.predecessors)):
            if n1.predecessors[i].key == n2.predecessors[j].key:
                lca.append(n1.predecessors[i].key)

    if not lca:
        if n1.key > n2.key:
            lca.append(dag_find_LCA(root, n1.predecessors[0], n2))
        else:
            lca.append(dag_find_LCA(root, n1, n2.predecessors[0]))

    return max(lca)








