public class BST {
    Node root;

    // constructor for the binary search tree
    BST(){
        this.root = null;
    }

    // calls the recursive insert function
    void insert(int value){
        root = insertRec(root, value);
    }

    // recursive function to insert the value
    Node insertRec(Node root, int value){
        // if the BST is empty
        if(root == null){
            root = new Node(value);
            return root;
        }

        // if the value is less than the root value, insert at root.left
        if(value<root.value){
            root.left = insertRec(root.left, value);
        }

        // if the value is greater than the root value, insert at root.right
        else if(value>root.value){
            root.right = insertRec(root.right, value);
        }

        // return root unchanged
        return root;
    }

    // will return the lowest common ancestor of x and y
    public static Node lowestCommonAncestor(Node root, Node x, Node y){
        BST bst = new BST();
        if(root==null){
            return null;
        }
        if((root.left==null&&root.right==null)||(bst.search(root, x.value)==null||bst.search(root, y.value)==null)){
            return root;
        }
        if(x.value < root.value && y.value < root.value) {
            return lowestCommonAncestor(root.left, x, y);
        }
        if(x.value > root.value && y.value > root.value) {
            return lowestCommonAncestor(root.right, x, y);
        }
        return root;
    }

    // A utility function to search a given key in BST
    public Node search(Node root, int value)
    {
        // Base Cases: root is null or key is present at root
        if (root==null || root.value==value)
            return root;

        // val is greater than root's key
        if (root.value > value)
            return search(root.left, value);

        // val is less than root's key
        return search(root.right, value);
    }

}
