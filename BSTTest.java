import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertNull;

import org.junit.Test;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
@RunWith(JUnit4.class)
public class BSTTest {
    @Test
    public void testLCA_rootNull(){
        BST tree = new BST();
        Node x = new Node(3);
        Node y = new Node(5);
        assertEquals(BST.lowestCommonAncestor(tree.root, x, y), null);
    }

  
}
