public class Node {
    int value;
    Node left;
    Node right;

    Node(int val){
        this.value = val;
        this.left = null;
        this.right = null;
    }
    public Node getLeft() {
        return left;
    }
    public Node getRight() {
        return right;
    }
}
