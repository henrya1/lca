from BST import *
import unittest


class TestBST(unittest.TestCase):
    def test_dag_LCA_null(self):
        r = None
        self.assertEqual(dag_find_LCA(r, 0, 0), None)

    def test_LCA_JustRoot(self):
        r = Node(1)
        self.assertEqual(dag_find_LCA(r, None, None), 1)

    def test_simple_dag(self):
        root = Node(1)
        node2 = Node(2)
        node3 = Node(3)
        node4 = Node(4)
        node5 = Node(5)
        node6 = Node(6)
        root.successors = [node2, node3, node4, node5]
        node2.successors = [node4]
        node2.predecessors = [root]
        node3.successors = [node4, node5]
        node3.predecessors = [root]
        node4.successors = [node5]
        node4.predecessors = [node2, node3, root]
        node5.predecessors = [node3, node4, root]
        node6.predecessors = [node4]

        lca = dag_find_LCA(root, node4, node5)

        self.assertEqual(lca, 3)

    def test_no_immediate_lca(self):
        root = Node(1)
        r2 = Node(2)
        r3 = Node(3)
        r4 = Node(4)
        r5 = Node(5)
        r6 = Node(6)
        root.successors = [r2,r3]
        r2.successors = [r4]
        r2.predecessors = [root]
        r3.successors = [r4, r5]
        r3.predecessors = [root]
        r4.successors= [r5]
        r4.predecessors = [r2]
        r5.predecessors = [r3]
        r6.predecessors = [r4]

        lca = dag_find_LCA(root, r4, r5)

        self.assertEqual(lca, 1)
        
    def test_root(self):
        root = Node(1)
        node2 = Node(2)
        node3 = Node(3)
        root.successors = [node2, node3]

        lca = dag_find_LCA(root, root, node3)
        self.assertEqual(lca, 1)

        lca = dag_find_LCA(root, node3, root)
        self.assertEqual(lca, 1)

        lca = dag_find_LCA(root, node2, node2)
        self.assertEqual(lca, 2)
